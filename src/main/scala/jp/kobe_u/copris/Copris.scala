package jp.kobe_u.copris

/** Trait for Copris DSL which provides methods for CSP and CSP solver.
  * This trait also provides implicit conversion of converting
  * scala Symbols to CSP integer variables ([[jp.kobe_u.copris.Var]]).
  */
trait CoprisTrait extends CSPTrait with SolverTrait {
  import scala.language.implicitConversions

  /** Implicit conversion from scala Symbol to [[jp.kobe_u.copris.Var]]. */
  implicit def symbol2var(s: Symbol): Var = Var(s.name)
  // /** Implicit conversion from scala Symbol to [[jp.kobe_u.copris.Constraint]]. */
  // implicit def symbol2constraint(s: Symbol) = Ne(Var(s.name), ZERO)
  /** CSP to be used */
  def csp: CSP

  /** Solver to be used */
  def solver: AbstractSolver

  /** Changes the solver to be used */
  def use(newSolver: AbstractSolver): Unit

  /** Gets the options of the solver */
  def options: collection.Map[String, String] = solver.options

  /** Sets the options of the solver */
  def setOptions(opts: Map[String, String]): Unit =
    solver.options = opts

  /** Initializes the CSP and solver */
  def init(): Unit = { csp.init(); solver.init() }
  /* */
  def commit(): Unit = { csp.commit(); solver.commit() }
  /* */
  def cancel(): Unit = { csp.cancel(); solver.cancel() }
  /* */
  def int(x: Var, d: Domain): Var = csp.int(x, d)
  /* */
  def int[A](x: Var, `enum`: EnumDomain[A]): Var = csp.int(x, 0, `enum`.size - 1)
  /* */
  def boolInt(x: Var): Var = csp.boolInt(x)
  /* */
  def bool(p: Bool): Bool = csp.bool(p)
  /* */
  def add(cs: Constraint*): Unit = csp.add(cs: _*)
  /* */
  def minimize(x: Var): Var = csp.minimize(x)
  /* */
  def maximize(x: Var): Var = csp.maximize(x)
  /* */
  def satisfiedBy(solution: Solution): Boolean = csp.satisfiedBy(solution)
  /* */
  def find: Boolean = solver.find
  /* */
  def findNext: Boolean = solver.findNext
  /* */
  def findOpt: Boolean = solver.findOpt

  /** Shows the CSP */
  def show(): Unit = print(csp.output)
  /* */
  def dump(fileName: String): Unit = { solver.dump(fileName, "") }
  /* */
  def dump(fileName: String, format: String): Unit = {
    solver.dump(fileName, format)
  }

  /** Returns the current solution */
  def solution: Solution = solver.solution

  /** Returns the iterator of all solutions */
  def solutions: Iterator[Solution] = solver.solutions

  /** Starts the timer (experimental) */
  def startTimer(timeout: Long): Unit = solver.startTimer(timeout)

  /** Stops the timer (experimental) */
  def stopTimer(): Unit = solver.stopTimer()

  /** Returns the info of the solver (experimental) */
  def info: collection.Map[String, String] = solver.solverInfo

  /** Returns the status of the solver (experimental) */
  def stats
      : collection.Seq[collection.Map[String, collection.Map[String, Number]]] =
    solver.solverStats
}

/** Class for Copris DSL which provides methods for CSP and CSP solver.
  * @constructor Constructs Copris with the given CSP and solver
  */
class Copris(val csp: CSP, var solver: AbstractSolver) extends CoprisTrait {

  /** Constructs Copris with the given CSP and [[jp.kobe_u.copris.DefaultSolver]] */
  def this(csp: CSP) =
    this(csp, DefaultSolver(csp))

  /** Constructs Copris with empty CSP and [[jp.kobe_u.copris.DefaultSolver]] */
  def this() =
    this(CSP())

  /** Changes the solver to be used */
  def use(newSolver: AbstractSolver): Unit =
    solver = newSolver
}

/** Object for Copris DSL which provides methods for CSP and CSP solver.
  */
object dsl extends CoprisTrait {

  /** Dynamic variable of Copris */
  val coprisVar = new util.DynamicVariable[CoprisTrait](new Copris())

  /** Returns Copris object */
  def copris: CoprisTrait = coprisVar.value

  /** Returns CSP */
  def csp: CSP = coprisVar.value.csp

  /** Returns CSP solver */
  def solver: AbstractSolver = coprisVar.value.solver
  /* */
  def use(newSolver: AbstractSolver): Unit =
    coprisVar.value.use(newSolver)

  /** Executes the `block` under the specified Copris */
  def using(copris: CoprisTrait = new Copris())(block: => Unit): Unit =
    coprisVar.withValue(copris) { block }
}
