NAME = copris
VERSION = 2.3.2

JAR = $(NAME)_2.12-$(VERSION).jar
JARALL = $(NAME)-all_2.12-$(VERSION).jar
SUGAR = lib/sugar-2.3.4.jar
SAT4J = lib/sat4j-pb.jar
PROPERTIES = log4j.properties
CP = lib/"*"
SRCS = src/main/scala/jp/kobe_u/*.scala src/main/scala/jp/kobe_u/copris/*.scala src/main/scala/jp/kobe_u/copris/*/*.scala
DOCS = docs/LICENSE.txt docs/CHANGES.org docs/api
INCLUDES = Makefile src/ examples/ $(PROPERTIES)
WEBPAGE = https://cspsat.gitlab.io/copris/
WEBTITLE = Copris: Constraint Programming in Scala

DOCTITLE = Copris version $(VERSION) Core API Specification
SCALADOC  = scaladoc \
	-d docs/api \
	-doc-title '$(DOCTITLE)' \
	-doc-version '$(VERSION)' \
	-classpath $(JAR):$(CP) \
	-sourcepath src/main/scala

all: jar scaladoc

jar: build/$(JAR) build/$(JARALL)

scaladoc: docs/api/index.html

build/$(JAR): $(SRCS) $(SUGAR) $(SAT4J)
	mkdir -p build
	mkdir -p classes
	rm -rf classes/*
	scalac -sourcepath src/main/scala -d classes -cp $(CP) $(SRCS)
	jar cf build/$(JAR) -C classes .
	jar uf build/$(JAR) $(PROPERTIES)
	jar uf build/$(JAR) meta-inf
	rm -rf classes/*

build/$(JARALL): build/$(JAR)
	cd classes; jar xf ../build/$(JAR) jp
	cd classes; jar xf ../$(SUGAR) jp
	cd classes; jar xf ../$(SAT4J)
	cd classes; rm -rf META-INF meta-inf
	cd classes; jar cf ../build/$(JARALL) *
	jar uf build/$(JARALL) $(PROPERTIES)
	rm -rf classes/*	

docs/api/index.html: build/$(JAR)
	rm -rf docs/api/*
	$(SCALADOC) $(SRCS)

install: build/$(JAR) copris coprisc
	cp -p copris coprisc /usr/local/bin/
	mkdir -p /usr/local/lib/copris
	rm -rf /usr/local/lib/copris/*.jar
	cp -p build/$(JAR) lib/*.jar /usr/local/lib/copris/

clean:
	rm -rf classes/*
	rm -rf docs/api/*
	rm -f build/$(JAR) build/$(JARALL)
